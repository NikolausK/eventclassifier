# eventClassifier
Classifier(s) predicting events using Gradient Boosting method.


## Description
Given Geographical coordinates (latitude, longitude):
i. Identify high risk areas (the driver needs to brake).
ii. Recognize/Distinguish drivers by their driving behaviour/habbits.